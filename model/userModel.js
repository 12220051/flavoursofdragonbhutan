
const mongoose = require("mongoose");
const validator = require('validator')
const UserSchema = new mongoose.Schema({
    email:{
        type:String,
        required:[true,'email is required'],
        trim:true,
        unique:true,
        lowercase:true,
        validate:[validator.isEmail,'please provide a valid email']
    },
    userName:{
        type:String,
        trim:true,
    },
    password:{
        type:String,
        required:true,
        min:8,
        max:64,
        select:false,
    },
    photo:{
        link:{
            type:String,
        },
        alt:{
            type:String,
        }
    },
    feedback:{
        type:String,
    },
    resetToken:{
        type:String,
    },
    // confirmPassword:{
    //     type:String,
    //     required:[true,'You need to confirm your password'],
    //     validate:{
    //         validator:function(el){
    //             return el === this.password;
    //         },
    //         message:"Password doesn't match"
    //     }
    // },
    role:{
        type:Number,
        default:0,
    }
})

module.exports = mongoose.model("User",UserSchema);
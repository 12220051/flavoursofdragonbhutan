

const {hashPassword,comparePassword} = require("../helper/hash");
const User = require("../model/userModel");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();
const register = async (req,res) =>{
    try{
        const {email,userName,password,confirmPassword,role} = req.body;
        if(!email.trim()){
            return res.status(400).json({msg:"email is required"})
        }
        if(!userName.trim()){
            return res.status(400).json({msg:"userName is must"})
        }
        if(!password.trim()){
            return res.status(400).json({msg:"password is must"})
        }
        if(!confirmPassword.trim()){
            return res.status(400).json({msg:"password must be confirmed"})
        }
        const hashedPassword = await hashPassword(password);
        const user = await new User({email,userName,password:hashedPassword}).save();
        // const token = jwt.sign({_id:user._id},process.env.JWT_SECRET,{expiresIn:'8d'})
        res.status(200).json({
            user:{
                email:user.email,
                userName:user.userName,
                role:user.role
            },
            // token
        })
    }catch(e){
        console.log("catch error",e);
        res.status(500).json({msg:e.message})
    }
}

const login = async (req,res) =>{
    try{
        const {email,password} = req.body;
        console.log(email,password)
        if(!email.trim()){
            return res.status(400).json({msg:"email is necessary"})
        }
        if(!password.trim()){
            return res.status(400).json({msg:"password is necessary"})
        }
        const user = await User.findOne({email}).select("+password");
        if(!user){
            return res.status(400).json({msg:"invalid login, try again"})
        }
        const result = await comparePassword(password,user.password)
        if(!result){
            return res.status(400).json({msg:"invalid login, try again"})
        }
        const token = jwt.sign({_id:user._id},process.env.JWT_SECRET,{expiresIn:"8d"})
        res.status(200).json({
            user:{
                email:user.email,
                userName:user.userName,
                role:user.role
            },
            token
        })
    }catch(e){
        console.log("catch error",e);
        res.status(500).json({msg:e.message})
    }
}

module.exports = {register,login}
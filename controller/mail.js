
const User = require("../model/userModel");
const jwt = require('jsonwebtoken');
const dotenv = require("dotenv");
const {sendMail} = require("../helper/mail");
const {hashPassword} = require("../helper/hash");
dotenv.config({path:"./config.env"});
const getResetLink = async (req,res) =>{
    try{
        const email = req.params.email;
        console.log("hello")
        const user = await User.findOne({email});
        if(!user){
            return res.status(404).json({msg:"the email is not registered with us. Try signing up!"})
        }
        const resetToken = jwt.sign({email:user.email},process.env.JWT_SECRET_RESET,{expiresIn:process.env.JWT_SECRET_RESET_EXPIRES_IN});
        user.resetToken = resetToken;
        console.log(resetToken,"reset token");
        await user.save();
        console.log(user)
        sendMail(resetToken,user.email,res);
        
    }catch(e){
        console.log("error catch",e.message)
        res.status(500).json({msg:e.message})
    }
}
const resetPassword = async (req,res) =>{
  
    try{
        const {newPassword,confirmPassword,resetToken} = req.body;
        console.log(resetToken)
        const decode = jwt.verify(resetToken,process.env.JWT_SECRET_RESET)
        const email2 = decode.email;
        console.log(email2)
        
        if(!newPassword){
            return res.status(400).json({msg:"newPassword is must"})
        }else if(!confirmPassword){
            return res.status(400).json({msg:"password musht be confirmed"})
        }else if(newPassword != confirmPassword){
            return res.status(400).json({msg:"new password and confirm password doesn't match"})
        }
        const hashedPassword = await hashPassword(newPassword);
        const user1 = await User.findOneAndUpdate({email:email2},{password:hashedPassword});
        if(!user1){
            return res.status(400).json({msg:"couldn't update password"});
        }
        console.log("successful")
        res.status(200).json({success:"successfully changed password"});

    }catch(e){
        if(e.name === 'TokenExpireError'){
            res.status(401).json({msg:'Token has expired'});
        }else if(e.name === 'JsonWebTokenError'){
            res.status(403).json({msg:"Invalid token"})
        }else{
            console.log("Error verifying JWT:",e);
            res.status(500).json({msg:"Internal Server Error"});
        }
    }

}
module.exports = {getResetLink,resetPassword};
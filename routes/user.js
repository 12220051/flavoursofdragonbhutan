
const express = require("express");
const router = express.Router();
const {register,login} = require("../controller/user");
const { requireSigning, isAdmin } = require("../middleware/auth");
router.route("/register").post( register)
router.route("/login").post( login)

module.exports = router;

const express = require("express");

const router = express.Router();
const {getResetLink,resetPassword} = require("../controller/mail")
router.route("/get-link/:email").post(getResetLink);
router.route("/change-password").patch(resetPassword);
module.exports = router;
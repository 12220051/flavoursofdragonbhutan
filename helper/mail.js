
const nodemailer = require('nodemailer');
const {google} = require("googleapis");
const dotenv = require("dotenv")
dotenv.config({path: "./config.env"});



const CLIENT_ID2 = process.env.CLIENT_ID2;
const CLIENT_SECRET2 = process.env.CLIENT_SECRET2;
const REDIRECT_URI2 = process.env.REDIRECT_URI2;
const REFRESH_TOKEN2 = process.env.REFRESH_TOKEN2;
const oAuth2Client = new google.auth.OAuth2(CLIENT_ID2,CLIENT_SECRET2,REDIRECT_URI2)
oAuth2Client.setCredentials({refresh_token:REFRESH_TOKEN2})

const sendMail = async(token,email,res) =>{
    try{
        console.log("hello")

        const accessToken = await oAuth2Client.getAccessToken()
        console.log(accessToken)
        const resetLink = `http://localhost:5000/resetpassword.html?token=${token}`
        console.log(CLIENT_ID2,CLIENT_SECRET2,REDIRECT_URI2,REFRESH_TOKEN2,accessToken)
        const transporter = nodemailer.createTransport({
            service:'gmail',
            auth:{
                type:'OAuth2',
                user:process.env.EMAIL,
                clientId:CLIENT_ID2,
                clientSecret:CLIENT_SECRET2,
                refreshToken:REFRESH_TOKEN2,
                accessToken:accessToken,
            },
            html:true,
        });

        const emailTemplate = `
            <h1>Hello</h1>
            <p>You have requested to reset your password. Please click the following link to reset your password:</p>
            <a href="${resetLink}">Reset Password</a>
        `;

        const mailOptions = {
            from:process.env.EMAIL,
            to:email,
            subject:'Password reset',
            html:emailTemplate,
        }
        transporter.sendMail(mailOptions,(error,info)=>{
            if(error){
                console.log("this is error boss")
                console.log('Error sending email:',error);
                return res.status(500).json({success:false,message:'Email could not be sent'});
            }else{
                console.log('Email sent:',info.response);
                return res.status(200).json({success:true,message:'Email sent successfully'});
            }
        });
    }catch(er){
        console.log(er)
        res.status(500).json({msg:"error in sending mail"+er})
    }
}
module.exports = {sendMail};

//Replace with your OAuth 2.0 client ID and client secret
// const CLIENT_ID = process.env.CLIENT_ID;
// const CLIENT_SECRET = process.env.CLIENT_SECRET;
// const REDIRECT_URI = process.env.REDIRECT_URI;

//2


// var data = {
//     email:"",
//     token:null
// }
// const sendMail = (token,email,res)=>{
//     // email = email;
//     // token = token
//     data.email = email
//     data.token = token
//     console.log(data)
//     console.log({"email":email,"token":token})
//     //CREATE AN OAuth2 client
//     oauth2Client = new OAuth2(
//         CLIENT_ID,
//         CLIENT_SECRET,
//         REDIRECT_URI
//     );

//     //generate an authentication URL
//     const authUrl = oauth2Client.generateAuthUrl({
//         access_type:'offline',
//         scope:'https://mail.google.com',
//     });
//     console.log('Authorize this app by visiting this URL:',authUrl);
//     // res.redirect(authUrl);
//     res.json({link:authUrl})

//     // console.log(process.env.EMAIL)
   
// }
// const sendMail2 = async (req,res) =>{
//     const {code} = req.query;
//     console.log(data);
//     try{
//         const {tokens} = await oauth2Client.getToken(code);
//         oauth2Client.setCredentials(tokens);

//         //Nodemailer setup
//         const resetToken = data.token;
        
//         console.log("token",data.token)
//         const resetLink = `http://localhost:5000/resetPassword.html?token=${resetToken}?${data.email}`
//         const transporter = nodemailer.createTransport({
//             service:'gmail',
//             auth:{
//                 type:'OAuth2',
//                 user:process.env.EMAIL,
//                 clientId:process.env.CLIENT_ID,
//                 clientSecret:process.env.CLIENT_SECRET,
//                 refreshToken:tokens.refresh_token,
//                 accessToken:tokens.access_token,
//             },
//             html:true,
//         });

//         const emailTemplate = `
//             <h1>Hello</h1>
//             <p>You have requested to reset your password. Please click the following link to reset your password:</p>
//             <a href="${resetLink}">Reset Password</a>
//         `;

//         const mailOptions = {
//             from:process.env.EMAIL,
//             to:data.email,
//             subject:'Password reset',
//             html:emailTemplate,
//         }

//         transporter.sendMail(mailOptions,(error,info)=>{
//             if(error){
//                 console.error('Error sending email:',error);
//                 return res.status(500).json({success:false,message:'Email could not be sent'});
//             }else{
//                 console.log('Email sent:',info.response);
//                 return res.status(200).json({success:true,message:'Email sent successfully'});
//             }
//         });
//     }catch(error){
//         console.error("Error exchanging code for tokens:",error);
//         res.status(500).send('Error exchanging code for tokens');
//     }
// }
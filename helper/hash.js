
const bcrypt = require("bcryptjs");


const hashPassword = (password) =>{
    return new Promise((resolve,reject) =>{
        bcrypt.genSalt(12,(err,salt)=>{
            if(err){
                reject(err);
            }
            bcrypt.hash(password,salt,(err,hashed)=>{
                if(err){
                    reject(err);
                }
                resolve(hashed);
            })
        })
    })
}

const comparePassword = async (password,hashedPassword) =>{
    console.log(password,hashedPassword)
    const t = bcrypt.compare(password,hashedPassword)
    console.log(t,"inside compare")
    return t;
}

module.exports = {hashPassword,comparePassword};
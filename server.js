


const mongoose = require("mongoose");
const app = require("./app");
const dotenv = require("dotenv");

const port = process.env.PORT || 5000;
dotenv.config({path:"./config.env"})
mongoose.connect(process.env.DATABASE).then((con) =>{
    console.log(con)
    console.log("connected to the database")
    app.listen(port,() =>console.log(`server is running on port ${port}`));
})
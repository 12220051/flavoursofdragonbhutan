
const passwordForm = document.getElementById("passwordForm");
const queryParams = new URLSearchParams(window.location.search);
const token = queryParams.get('token');
console.log(token)
const changePassword = (e)=>{
    e.preventDefault();
    console.log(token)
    const data = {
        newPassword: document.getElementById("newPassword").value,
        confirmPassword: document.getElementById("confirmPassword").value,
        resetToken:token
    }
    if(!data.newPassword){
        alert("Set a new Password");
    }else if(!data.confirmPassword){
        alert("Confirm New password")
    }else if(data.newPassword != data.confirmPassword){
        alert("Password doesn't match");
    }
    fetch("/forgot-password/change-password",{
        method:"PATCH",
        headers:{"Content-Type":"application/json; charset=utf-8"},
        body:JSON.stringify(data)
    })
    .then((res) => {
        return res.json()
    })
    .then((res2) =>{
        if(res2.success){
            window.location.href = "login.html";
        }else {
            alert(res2.msg)
        }
    })
    .catch((e) =>{
        alert(e.message);
    })
    
}
passwordForm.addEventListener('submit',changePassword);
const resetForm = document.getElementById("resetForm");
const resetPassword = (e)=>{
    e.preventDefault();
    const email = document.getElementById("email").value;
    console.log("hiiiiii")
    fetch("/forgot-password/get-link/"+email,{
        method:"POST",
        headers:{ "Content-Type": "application/json; charset=UTF-8" },
        body:JSON.stringify({})
    })
    .then((res)=>res.text())
    .then((data)=>showMessage(data))

}
const showMessage = () =>{
    const forgotPasswordBtn = document.getElementById('submitbtn');
     const popup = document.getElementById('popup');
     const closePopup = document.getElementById('closePopup');
     
     forgotPasswordBtn.addEventListener('click', () => {
         popup.style.display = 'block';
     });
     
     closePopup.addEventListener('click', () => {
         popup.style.display = 'none';
     });
     document.getElementById("email").value = "";
}
resetForm.addEventListener("submit",resetPassword);

// const forgotPasswordBtn = document.getElementById('forgotPasswordBtn');
// const popup = document.getElementById('popup');
// const closePopup = document.getElementById('closePopup');

// forgotPasswordBtn.addEventListener('click', () => {
//     popup.style.display = 'block';
// });

// closePopup.addEventListener('click', () => {
//     popup.style.display = 'none';
// });

var loginEmailInput = document.getElementById("loginEmail");
var loginEmailError = document.getElementById("loginEmailError");

var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

loginEmailInput.addEventListener("blur", function() {
    var email = loginEmailInput.value;
    if (!emailRegex.test(email)) {
        loginEmailError.textContent = "Invalid email address.";
    } else {
        loginEmailError.textContent = "";
    }
});

var emailInput = document.getElementById("signupEmail");
var passwordInput = document.getElementById("signupPassword");
var confirmPasswordInput = document.getElementById("confirmPassword");
var userNameInput = document.getElementById("userName");
var userNameError = document.getElementById("userNameError");
var emailError = document.getElementById("emailError");
var passwordError = document.getElementById("passwordError");
var confirmPasswordError = document.getElementById("confirm_passwordError");

var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

emailInput.addEventListener("blur", function() {
    var email = emailInput.value;
    if (!emailRegex.test(email)) {
        emailError.textContent = "Invalid email address.";
    } else {
        emailError.textContent = "";
    }
});

userNameInput.addEventListener("blur",() =>{
    var userName = userNameInput.value;
    if(!userName){
        userNameError.textContent = "Name Cannot Be Empty"
    }else{
        userNameError.textContent = "";
    }
});
passwordInput.addEventListener("blur", function() {
    var password = passwordInput.value;
    if (password.length < 8) {
        passwordError.textContent = "Password must be at least 8 characters long.";
    } else {
        passwordError.textContent = "";
    }
});

confirmPasswordInput.addEventListener("blur", function() {
    var confirmPassword = confirmPasswordInput.value;
    var password = passwordInput.value;
    if (password !== confirmPassword) {
        confirmPasswordError.textContent = "Passwords do not match.";
    } else {
        confirmPasswordError.textContent = "";
    }
});

const loginForm = document.getElementById("loginForm")
const signupForm = document.getElementById("signupForm");
const loginRequest = (e)=>{
    const data = {
        email: document.getElementById("loginEmail").value,
        password:document.getElementById("loginPassword").value
    }
    e.preventDefault();

    fetch("/user/login",{
        method:"POST",
        body:JSON.stringify(data),
        headers:{ "Content-Type": "application/json; charset=UTF-8" }
    }).then((res) => {
       return  res.json()
    }).then((res2) =>{
        console.log(res2,"kdfsdfksjfd")
        if(res2.msg){
            console.log(res2)
            alert(res2.msg);
        }else{
            if(res2.user.role === 1){
                window.location.href = "receipe-post.html"
            }else{
                window.location.href = "index.html"
            }
        }
    }).catch((e)=>{
        alert(e);
    })
}

const signupRequest = () =>{
    const data = {
        userName: document.getElementById("userName").value,
        email: document.getElementById("signupEmail").value,
        password: document.getElementById("signupPassword").value,
        confirmPassword:document.getElementById("confirmPassword").value
    }
    fetch("/user/register",{
        method:"POST",
        body:JSON.stringify(data),
        headers:{"Content-Type":"Application/json; Charset=UTF-8"}
    }).then((res) =>{
        return {
            status:res.status,
            data:res.json()
        }
    }).then((res2) =>{
        const status = res2.status
        const user = res2.data
        if(status === 201){
            window.location.href = "login.html"
            document.getElementById("loginEmail").textContent = user.email;
        }else{
            alert(user.msg)
        }
    }).catch((e) =>{
        alert(e)
    })
}
loginForm.addEventListener('submit',loginRequest);
signupForm.addEventListener("submit",signupRequest);

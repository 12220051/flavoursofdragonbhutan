

const express = require("express");
const app = express();
const cors = require("cors")
const userRouter = require("./routes/user")
const mailRouter = require("./routes/mail");
const {sendMail2} = require("./helper/mail");
app.use(express.json())
app.use(express.static('view'))

app.use("/user",userRouter)
app.use("/forgot-password",mailRouter);
// app.use("/reset-password",)
// app.get('/oauth2callback',sendMail2)
module.exports = app;